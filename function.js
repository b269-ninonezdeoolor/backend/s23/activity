	/*
	1. Create a trainer object using object literals.

	2. Initialize/add the following trainer object properties:
		a. Name (String)
		b. Age (Number)
		c. Pokemon (Array)
		d. Friends (Object with Array values for properties)

	3. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!

	4. Access the trainer object properties using dot and square bracket notation.

	5. Invoke/call the trainer talk object method.

	6. Create a constructor for creating a pokemon with the following properties:
		a. Name (Provided as an argument to the contructor)
		b. Level (Provided as an argument to the contructor)
		c. Health (Create an equation that uses the level property)
		d. Attack (Create an equation that uses the level property)

	7. Create/instantiate several pokemon object from the constructor with varying name and level properties.

	8. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.

	9. Create a faint method that will print out a message of targetPokemon has fainted.

	10. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.

	11. Invoke the tackle method of one pokemon object to see if it works as intended.
	*/

	const trainer = {
	    name: "Ash Ketchum",
	    age: 10,
	    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	    friends: {
	        haenn: ["May", "Max"],
	        kanto: ["Brock", "Misty"]
	    },
	    talk: function() {
	        console.log("Pikachu! I choose you!");
	    }
	};

	console.log(trainer);
	console.log("Result of dot notation:");
	console.log(trainer.name);
	console.log("Result of square bracket notation:");
	console.log(trainer["pokemon"]);
	console.log("Result of talk method");
	trainer.talk();

	function Pokemon(name, level)
	{
		this.name = name;
		this.level = level;
		this.health = level * 2;
		this.attack = level;

		this.tackle = function(targetPokemon)
		{
			targetPokemon.health -= this.attack;

			console.log(this.name + " tackled " + targetPokemon.name + "!");
			console.log(targetPokemon.name + "'s health is now reduced to : " + targetPokemon.health);
			console.log(this.name + "'s health is now reduced to : " + this.health);

			if (targetPokemon.health <= 0)
			{
				this.faint(targetPokemon);
			}
		};
			this.faint = function(pokemon)
			{
				console.log(pokemon.name + " fainted!");
			};
	}

	const pikachu = new Pokemon("Pikachu", 12);
	const geodude = new Pokemon("Geodude", 8);
	const mewtwo = new Pokemon("Mewtwo", 100);

	console.log(pikachu);
	console.log(geodude);
	console.log(mewtwo);

	pikachu.tackle(geodude);
	pikachu.tackle(mewtwo);
	geodude.tackle(mewtwo);
	geodude.tackle(pikachu);
	mewtwo.tackle(pikachu);
	mewtwo.tackle(geodude);